PHONY: clean help

BUILD_DIR       =build
COMMON          =
DEBUG           =-DCMAKE_BUILD_TYPE=Debug
RELEASE         =-DCMAKE_BUILD_TYPE=Release
MAKEFILE        =-G"Unix Makefiles" -DCMAKE_VERBOSE_MAKEFILE=ON
NINJA           =-G"Ninja"
GCC             =-DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_COMPILER=gcc
CLANG           =-DCMAKE_CXX_COMPILER=clang++ -DCMAKE_C_COMPILER=clang
VS2017          =-G"Visual Studio 15 2017 Win64"

clean:                  ## Remove build directory.
	rm -rf $(BUILD_DIR)

help:                   ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'	

linux:                  ## Custom Configure & Build - args: [COMPILER=<GCC|CLANG>] [BUILD=<MAKEFILE|NINJA>] [TYPE=<DEBUG|RELEASE>].
	mkdir -p $(BUILD_DIR)/$(COMPILER)-$(BUILD)-$(TYPE);\
	cd $(BUILD_DIR)/$(COMPILER)-$(BUILD)-$(TYPE);\
	cmake ../.. $(COMMON) $($(COMPILER)) $($(BUILD)) $($(TYPE));\
	make;\
	ninja

gcc-ninja-debug:        ## Configure and build GCC/Ninja/Debug.
gcc-ninja-debug: $(eval COMPILER=GCC) $(eval BUILD=NINJA) $(eval TYPE=DEBUG) linux

gcc-make-debug:         ## Configure and build GCC/Make/Debug.
gcc-make-debug: $(eval COMPILER=GCC) $(eval BUILD=MAKEFILE) $(eval TYPE=DEBUG) linux

gcc-ninja-release:      ## Configure and build GCC/Ninja/Release.
gcc-ninja-release: $(eval COMPILER=GCC) $(eval BUILD=NINJA) $(eval TYPE=RELEASE) linux

clang-ninja-debug:      ## Configure and build Clang/Ninja/Debug.
clang-ninja-debug: $(eval COMPILER=CLANG) $(eval BUILD=NINJA) $(eval TYPE=DEBUG) linux

msvc-vs:                ## Configure Visual Studio 2017 Solution (QTENV) (VCVARS_BAT).
	mkdir -p $(BUILD_DIR)/$@;\
	cd $(BUILD_DIR)/$@;\
	$(QTENV)/bin/qtenv2.bat;\
	$(VCVARS_BAT);\
	cmake ../.. $(COMMON) $(VS2017) -DCMAKE_PREFIX_PATH="$(QTENV)"
