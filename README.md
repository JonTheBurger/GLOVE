# GNU Linker Output Viewer & Editor
A graphical front-end for objdump and readelf for viewing the contents of elf files, map files, and linker definition scripts. Because it uses QtCharts, it is licensed under the GPLv3 license.
Check out the [Trello Board](https://trello.com/b/JQGObFWr/glove).

## Setup
- Clone the repository _recursively_
- (Visual Studio) Run <install-location>/Qt/<qt-version>/<msvc-version>/bin/qtenv2.bat
- (Visual Studio) Run <install-location>/Microsoft Visual Studio/2017/Community/Common7/Tools/vsdevcmd/ext/vcvars.bat
- (Visual Studio) Run cmake <source-path> -DCMAKE_PREFIX_PATH="<install-location>/Qt/<qt-version>/<msvc-version>" -G"Visual Studio 15 2017 Win64"
- (Visual Studio) Build the Qt<module>.dll targets
- (Linux) Run a build configuration from the makefile, e.g.
```
make gcc-make-debug
```

## Requires
- CMake
- C++17 Compiler (GCC, Clang, or MSVC)
- Make or Ninja
- Qt5 (Core Charts Gui Sql Widgets)
- Git (Submodules Catch and Trompeloeil)

## Attributions
[QDarkStyleSheet](https://github.com/ColinDuquesnoy/QDarkStyleSheet) [MIT]
