#pragma once

#include <QString>

#include "SectionHeader.hxx"
#include "Symbol.hxx"

namespace glove {

class Tooltips {
public:
  const QString& FlagDescription(SectionHeaderFlags flag) const;
  const QString& FlagDescription(Symbol::Flag0 flag) const;
  const QString& FlagDescription(Symbol::Flag1 flag) const;
  const QString& FlagDescription(char flag) const;
};

} // namespace glove
