#include <QList>
#include <QRegularExpression>
#include <QString>
#include <QStringRef>
#include <QVector>

#include "Symbol.hxx"
#include "SymbolTableParser.hxx"

namespace glove {

QStringList::ConstIterator SkipHeader(const QStringList& lines) {
  static const auto HEADER = QStringLiteral("Name                  Value           Class        Type         Size             Line  Section");
  auto&&            it     = lines.cbegin();
  while (it != lines.cend() && *it != HEADER) {
    ++it;
  }
  ++it;
  return it;
}

Symbol FromLine(const QString& line) {
  // "__bss_start         |0000000000601064|   B  |            NOTYPE|                |     |.bss"
}

QVector<Symbol> SymbolTableParser::FromNmOutput(const QString& dump) {
  auto&& lines    = dump.split(QRegExp("\n|\r\n|\r"), QString::SkipEmptyParts);
  auto&& lineIter = SkipHeader(lines);

  while (lineIter != lines.cend() && lineIter->contains("[")) {
    QString line = *lineIter;
    ++lineIter;
    //    auto sectionHeader = FromEntry(readelfFirstLine, readelfSecondLine);
    //    table->push_back(sectionHeader);
  }
}

} // namespace glove
