#include "Settings.hxx"

namespace glove {

Settings::Settings(QSettings* qsettings)
    : qsettings_(qsettings) {}

Settings::~Settings() {
  qsettings_->sync();
}

void Settings::LoadWidgetSizePos(QWidget* widget) const {
  qsettings_->beginGroup(widget->objectName());
  widget->resize(qsettings_->value("size", widget->size()).toSize());
  widget->move(qsettings_->value("pos", widget->pos()).toPoint());
  qsettings_->endGroup();
}

void Settings::SaveWindowSizePos(const QWidget& widget) {
  qsettings_->beginGroup(widget.objectName());
  qsettings_->setValue("size", widget.size());
  qsettings_->setValue("pos", widget.pos());
  qsettings_->endGroup();
}

} // namespace glove
