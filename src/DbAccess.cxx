#include <QDebug>
#include <QSqlError>
#include <QSqlQuery>
#include <QString>
#include <QVariant>
#include <QVector>

#include "DbAccess.hxx"

namespace glove {

DbAccess::DbAccess(QSqlDatabase* db)
    : db_(db) {}

DbAccess::~DbAccess() {
  if (db_->isOpen()) { db_->close(); }
}

bool DbAccess::Open(const QString& name) {
  if (db_->isOpen() && db_->databaseName() != name) { db_->close(); }

  db_->setDatabaseName(name);
  bool success = db_->open();
  if (!success) { qDebug() << "ERROR: Database open failure"; }
  return success;
}

void DbAccess::Write(const QVector<SectionHeader>& headers) {
  db_->exec("DROP TABLE IF EXISTS SectionHeaders");
  db_->exec("CREATE TABLE SectionHeaders "
            "idx       INT PRIMARY KEY AUTOINCREMENT, "
            "name      TEXT NOT NULL, "
            "size      INT NOT NULL, "
            "vma       INT NOT NULL, "
            "lma       INT NOT NULL, "
            "fileOff   INT NOT NULL, "
            "flags     INT NOT NULL, "
            "alignment INT NOT NULL");

  for (auto&& header : headers) {
    QSqlQuery query(*db_);

    // TODO: Update DB
    query.prepare("INSERT INTO SectionHeaders (name, size, vma, lma, fileOff, flags, alignment) "
                  "VALUES (:name, :size, :vma, :lma, :fileOff, :flags, :alignment)");
    query.bindValue(":name", header.name);
    query.bindValue(":size", QVariant::fromValue(header.size));
    query.bindValue(":vma", QVariant::fromValue(header.address));
    query.bindValue(":lma", QVariant::fromValue(header.link));
    query.bindValue(":fileOff", QVariant::fromValue(header.offset));
    query.bindValue(":flags", static_cast<std::uint32_t>(header.flags));
    query.bindValue(":alignment", header.alignment);

    if (!query.exec()) { qDebug() << "WARN:" << query.lastError().text(); }
  }
}

void DbAccess::Write(const QVector<Symbol>& symbolTable) {
  QSqlQuery dropQuery(*db_);
  dropQuery.prepare("DROP TABLE IF EXISTS Symbols");
  if (!dropQuery.exec()) { qDebug() << "WARN: DropQuery" << dropQuery.lastError() << '\n'; }

  QSqlQuery createQuery(*db_);
  createQuery.prepare("CREATE TABLE Symbols ("
                      "name    TEXT PRIMARY KEY NOT NULL, "
                      "section TEXT NOT NULL, "
                      "address INT NOT NULL, "
                      "size    INT NOT NULL, "
                      "flag0   INT NOT NULL, "
                      "flag1   INT NOT NULL, "
                      "flag2   INT NOT NULL, "
                      "flag3   INT NOT NULL, "
                      "flag4   INT NOT NULL, "
                      "flag5   INT NOT NULL, "
                      "flag6   INT NOT NULL"
                      ");");
  if (!createQuery.exec()) { qDebug() << "WARN: CreateQuery" << createQuery.lastError() << '\n'; }

  if (!db_->transaction()) { qDebug() << "WARN: Transaction failure" << '\n'; }

  for (auto&& symbol : symbolTable) {
    QSqlQuery query(*db_);
    query.prepare("INSERT INTO Symbols (name, section, address, size, "
                  "flag0, flag1, flag2, flag3, flag4, flag5, flag6) "
                  "VALUES (:name, :section, :address, :size, "
                  ":flag0, :flag1, :flag2, :flag3, :flag4, :flag5, :flag6)");
    query.bindValue(":name", symbol.name);
    query.bindValue(":section", symbol.section);
    query.bindValue(":address", QVariant::fromValue(symbol.valueOrAddress));
    query.bindValue(":size", QVariant::fromValue(symbol.sizeOrAlignment));
    query.bindValue(":flag0", static_cast<char>(symbol.flag0));
    query.bindValue(":flag1", static_cast<char>(symbol.flag1));
    query.bindValue(":flag2", static_cast<char>(symbol.flag2));
    query.bindValue(":flag3", static_cast<char>(symbol.flag3));
    query.bindValue(":flag4", static_cast<char>(symbol.flag4));
    query.bindValue(":flag5", static_cast<char>(symbol.flag5));
    query.bindValue(":flag6", static_cast<char>(symbol.flag6));

    if (!query.exec()) { qDebug() << "WARN:" << query.lastError().text(); }
  }

  if (!db_->commit()) { qDebug() << "WARN: Commit failure" << '\n'; }
}

QSqlQuery DbAccess::SectionHeaderTableQuery() {
  return QSqlQuery("SELECT * FROM SectionHeaders", *db_);
}

QSqlQuery DbAccess::SymbolTableQuery() {
  return QSqlQuery("SELECT * FROM Symbols", *db_);
}

QSqlQuery DbAccess::AddressSizeQuery(const QString& functionName) {
  QSqlQuery query(*db_);
  query.prepare("SELECT address, size FROM Symbols WHERE name=?");
  query.addBindValue(functionName);
  return query;
}

} // namespace glove
