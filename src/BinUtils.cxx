#include <QProcess>

#include "BinUtils.hxx"
#include "SectionHeaderTableParser.hxx"
#include "SymbolTableParser.hxx"

namespace glove {

BinUtils::BinUtils(SectionHeaderTableParser* sectionParser,
                   SymbolTableParser*        symbolParser,
                   QObject*                  parent)
    : QObject(parent)
    , sectionParser_(sectionParser)
    , symbolParser_(symbolParser) {}

void BinUtils::ParseSectionHeaderTable(const QString& elfFile, QVector<SectionHeader>* table) {
  QProcess* readelf = new QProcess(this);
  connect(readelf,
          static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
          [this, table, readelf, &elfFile](int exitCode, QProcess::ExitStatus exitStatus) {
            if (exitCode == 0 && exitStatus == QProcess::ExitStatus::NormalExit) {
              QString dump = readelf->readAllStandardOutput();
              sectionParser_->FromReadelfOutput(dump, table);
            }
            emit SignalParseSectionHeaderTableFinished(elfFile);
          });
  readelf->start(readelfPath, { "-S", elfFile });
}

void BinUtils::ParseSymbolTable(const QString& elfFile, QVector<Symbol>* table) {
  QProcess* nm = new QProcess(this);
  connect(nm,
          static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
          [this, table, nm, &elfFile](int exitCode, QProcess::ExitStatus exitStatus) {
            if (exitCode == 0 && exitStatus == QProcess::ExitStatus::NormalExit) {
              QString dump = nm->readAllStandardOutput();
              *table       = symbolParser_->FromNmOutput(dump);
            }
            emit SignalParseSymbolTableFinished(elfFile);
          });
  nm->start(nmPath, { "-S", "-C", "-fsysv", elfFile });
}

void BinUtils::Disassemble(const QString& elfFile, std::uintptr_t address, std::uintptr_t size) {
  QProcess* objdump = new QProcess(this);
  connect(objdump,
          static_cast<void (QProcess::*)(int, QProcess::ExitStatus)>(&QProcess::finished),
          [this, objdump](int exitCode, QProcess::ExitStatus exitStatus) {
            QString dissAsm;
            if (exitCode == 0 && exitStatus == QProcess::ExitStatus::NormalExit) {
              dissAsm = objdump->readAllStandardOutput();
            }
            emit SignalDisassemblyFinished(dissAsm);
          });
  const QString startAddr = QString("--start-address=%1").arg(QString::number(address));
  const QString stopAddr  = QString("--stop-address=%1").arg(QString::number(address + size));
  objdump->start(objdumpPath, { "-CS", startAddr, stopAddr, elfFile });
}

} // namespace glove
