#include <QRegularExpression>
#include <QString>
#include <QStringRef>
#include <QVector>
#include <cmath>

#include "SectionHeader.hxx"
#include "SectionHeaderTableParser.hxx"

namespace glove {

static const QRegularExpression   Separators("( |,)");
static QStringList::ConstIterator SkipHeader(const QStringList& lines);

QVector<SectionHeader> SectionHeaderTableParser::FromReadelfOutput(const QString& dump) {
  QVector<SectionHeader> table;
  FromReadelfOutput(dump, &table);
  return table;
}

//readelf -S test/resources/simple.elf
//There are 36 section headers, starting at offset 0xeb18:

void SectionHeaderTableParser::FromReadelfOutput(const QString&          dump,
                                                 QVector<SectionHeader>* table) {
  auto&& lines    = dump.split(QRegExp("\n|\r\n|\r"), QString::SkipEmptyParts);
  auto&& lineIter = SkipHeader(lines);

  while (lineIter != lines.cend() && lineIter->contains("[")) {
    QString readelfFirstLine = *lineIter;
    ++lineIter;
    QString readelfSecondLine = *lineIter;
    ++lineIter;
    auto sectionHeader = FromEntry(readelfFirstLine, readelfSecondLine);
    table->push_back(sectionHeader);
  }
}

QStringList::ConstIterator SkipHeader(const QStringList& lines) {
  static const QString SectionHeaderTableHeader =
      "       Size              EntSize          Flags  Link  Info  Align";
  auto&& it = lines.cbegin();
  while (it != lines.cend() && *it != SectionHeaderTableHeader) {
    ++it;
  }
  ++it;
  return it;
}

SectionHeader SectionHeaderTableParser::FromEntry(const QString& line1, const QString& line2) {
  SectionHeader sectionHeader;
  //Section Headers:
  //  [Nr] Name              Type             Address           Offset
  //  [ 0]                   NULL             0000000000000000  00000000

  //       Size              EntSize          Flags  Link  Info  Align
  //       0000000000000000  0000000000000000           0     0     0
  //  [ 1] .interp           PROGBITS         0000000000400238  00000238
  //       000000000000001c  0000000000000000   A       0     0     1

  sectionHeader.name    = line1.mid(sizeof("  [Nr] ") - 1, sizeof("0000000000000000")).trimmed();
  sectionHeader.type    = line1.mid(sizeof("  [Nr] Name              ") - 1, sizeof("0000000000000000")).trimmed();
  sectionHeader.address = line1.midRef(sizeof("  [Nr] Name              Type             ") - 1, sizeof("0000000000000000")).toULongLong(nullptr, 16);
  sectionHeader.offset  = line1.midRef(sizeof("  [Nr] Name              Type             Address           ") - 1, sizeof("0000000000000000")).toULongLong(nullptr, 16);

  sectionHeader.size      = line2.midRef(sizeof("       ") - 1, sizeof("0000000000000000")).toULongLong(nullptr, 16);
  sectionHeader.entSize   = line2.midRef(sizeof("       Size              ") - 1, sizeof("0000000000000000")).toULongLong(nullptr, 16);
  QString flags           = line2.mid(sizeof("       Size              EntSize          ") - 1, sizeof("      "));
  sectionHeader.link      = line2.midRef(sizeof("       Size              EntSize          Flags  ") - 1, sizeof("   0")).toInt(nullptr, 16);
  sectionHeader.info      = line2.midRef(sizeof("       Size              EntSize          Flags  Link  ") - 1, sizeof("   0")).toInt(nullptr, 16);
  sectionHeader.alignment = line2.midRef(sizeof("       Size              EntSize          Flags  Link  Info  ") - 1).toInt(nullptr, 16);

  return sectionHeader;
}

SectionHeaderFlags SectionHeaderTableParser::FlagsFromLine(const QString& line) {
  SectionHeaderFlags sectionHeaderFlags = SectionHeaderFlags::None;
  const QStringList  tokens             = line.split(Separators, QString::SplitBehavior::SkipEmptyParts);

  for (auto&& token : tokens) {
    auto&& flag = FlagFromString(token);
    sectionHeaderFlags |= flag;
  }

  return sectionHeaderFlags;
}

SectionHeaderFlags SectionHeaderTableParser::FlagFromString(const QString& str) {
  static const QHash<const QString, SectionHeaderFlags> map = {
    { "CONTENTS", SectionHeaderFlags::Contents },
    { "ALLOC", SectionHeaderFlags::Alloc },
    { "LOAD", SectionHeaderFlags::Load },
    { "READONLY", SectionHeaderFlags::Readonly },
    { "CODE", SectionHeaderFlags::Code },
    { "DATA", SectionHeaderFlags::Data },
  };
  auto&& kvp = map.find(str);
  if (kvp != map.cend()) { return kvp.value(); }
  return SectionHeaderFlags::Unknown;
}

} // namespace glove
