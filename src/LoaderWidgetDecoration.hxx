#ifndef LOADERWIDGETDECORATOR_HXX
#define LOADERWIDGETDECORATOR_HXX

#include <QObject>
#include <QPointer>

class QGraphicsOpacityEffect;
class QProgressBar;
class QWidget;

class LoaderWidgetDecoration final : public QObject {
  Q_GADGET

public:
  explicit LoaderWidgetDecoration(QWidget* parent);
  bool eventFilter(QObject* object, QEvent* event) override;
  void RunLoader(bool isLoading);

private:
  void FitSize();

private:
  QWidget* const parent_;
  const QPointer<QProgressBar> bar_;
  const QPointer<QGraphicsOpacityEffect> opacity_;
};

#endif // LOADERWIDGETDECORATOR_HXX
