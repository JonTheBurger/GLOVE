#pragma once

#include <QString>
#include <QVector>

#include "Symbol.hxx"

namespace glove {

class SymbolTableParser {
public:
  QVector<QStringRef> SplitKeepDelimiters(const QString& string, const QVector<QChar>& delimiters);
  QVector<Symbol>     FromObjdumpOutput(const QString& dump);
  QVector<Symbol>     FromNmOutput(const QString& dump);
  Symbol              FromLine(const QString& line);
};

} // namespace glove
