#pragma once

#include <QList>
#include <QString>
#include <QVector>

#include "SectionHeader.hxx"

namespace glove {

class SectionHeaderTableParser {
public:
  QVector<SectionHeader> FromReadelfOutput(const QString& dump);
  void                   FromReadelfOutput(const QString& dump, QVector<SectionHeader>* table);
  SectionHeader          FromEntry(const QString& line1, const QString& line2);
  SectionHeaderFlags     FlagsFromLine(const QString& line);
  SectionHeaderFlags     FlagFromString(const QString& str);
};

} // namespace glove
