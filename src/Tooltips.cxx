#include <QObject>
#include <QString>
#include <map>

#include "SectionHeader.hxx"
#include "Tooltips.hxx"

namespace glove {

// Thanks to https://stackoverflow.com/questions/11196048/flags-in-objdump-output-of-object-file
const QString& Tooltips::FlagDescription(SectionHeaderFlags flag) const {
  static const QString                                     NoDescription = "";
  static const std::map<SectionHeaderFlags, const QString> map           = {
    { SectionHeaderFlags::Unknown,
      QObject::tr("<UNKNOWN> - This program does not recognize this a flag.") },
    { SectionHeaderFlags::Contents,
      QObject::tr("CONTENTS - Section has corresponding contents in executable. For example, .text "
                  "usually has contents in executable, while .bss usually does not.") },
    { SectionHeaderFlags::Alloc,
      QObject::tr("ALLOC - Section occupies memory. Memory pages are allocated to hold the section "
                  "content when a process is created. Some sections (such as those containing "
                  "debug info) are not read into memory during normal program execution, and are "
                  "therefore not marked ALLOC as to save memory.") },
    { SectionHeaderFlags::Load,
      QObject::tr("LOAD - Section resides in loadable segment. Its contents could be read from the "
                  "file into memory when a process is created.") },
    { SectionHeaderFlags::Readonly,
      QObject::tr("READONLY - Section is neither executable nor writable "
                  "and should be placed in read-only memory pages.") },
    { SectionHeaderFlags::Code, QObject::tr("CODE - Section contains executable code.") },
    { SectionHeaderFlags::Data, QObject::tr("DATA - Section is not executable but is writable.") },
  };
  auto&& kvp = map.find(flag);
  if (kvp != map.cend()) { return kvp->second; }
  return NoDescription;
}

// For most flags, a <space> means that the symbol is simply an "ordiary symbol".
// For flags 0 and 1, it specifically means Neither and Strong respectively.
const QString& Tooltips::FlagDescription(Symbol::Flag0 flag) const {
  // Check for Flag0 special case <space> character
  static const QString neitherStr =
      QObject::tr("Neither Local Nor Global ( ) - Can be neither local nor global for a "
                  "variety of reasons, for example  it is used in debugging");
  if (flag == Symbol::Neither) { return neitherStr; }
  return FlagDescription(static_cast<char>(flag));
}

const QString& Tooltips::FlagDescription(Symbol::Flag1 flag) const {
  // Check for Flag1 special case <space> character
  static QString strongStr =
      QObject::tr("Strong ( ) - Multiple definitions of strong symbols cause linker errors");
  if (flag == Symbol::Strong) { return strongStr; }
  return FlagDescription(static_cast<char>(flag));
}

const QString& Tooltips::FlagDescription(char flag) const {
  static const QString                       NoDescription = "";
  static const std::map<char, const QString> map           = {
    { 'l',
      QObject::tr("Local (l) - Defined and referenced exclusively by one translation unit,"
                  "for example static symbols") },
    { 'g', QObject::tr("Global (g) - Accessible from multiple translation units") },
    { '!', QObject::tr("Both Local And Global (!) - Probably an indication of a bug") },
    { 'u',
      QObject::tr("Unique Global (u) - GNU extension to the standard set of ELF symbol bindings") },
    { 'w',
      QObject::tr(
          "Weak (w) - Provides default implementation that can be overridden by a strong symbol") },
    { 'C', QObject::tr("Constructor (C) - Denotes a constructor") },
    { 'W',
      QObject::tr("Warning (W) - A warning symbol's name is a message to be displayed if the "
                  "symbol following the warning symbol is ever referenced") },
    { 'I', QObject::tr("Indirect Reference (I) - An indirect reference to another symbol") },
    { 'i', QObject::tr("Relocated (i) - Function to be evaluated during relocation processing") },
    { 'd', QObject::tr("Debug symbol (d) - A symbol containing debug information") },
    { 'D', QObject::tr("Dynamic symbol (D) - Used by the dynamic linker for identification") },
    { 'F', QObject::tr("Function name (F) - A symbol naming a function") },
    { 'f', QObject::tr("File name (f) - A symbol naming a file") },
    { 'O', QObject::tr("Object name (O) - A symbol naming an object") },
    { ' ', QObject::tr("Ordinary Symbol ( ) - Just a normal symbol") },
  };
  auto&& kvp = map.find(flag);
  if (kvp != map.cend()) { return kvp->second; }
  return NoDescription;
}

} // namespace glove
