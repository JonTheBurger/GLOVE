#pragma once

#include <QFuture>
#include <QObject>
#include <QString>
#include <QVector>
#include <cstdint>

#include "SectionHeader.hxx"
#include "Symbol.hxx"

namespace glove {

class SectionHeaderTableParser;
class SymbolTableParser;

class BinUtils : public QObject {
  Q_OBJECT

public:
  BinUtils(SectionHeaderTableParser* sectionParser, SymbolTableParser* symbolParser, QObject* parent = nullptr);
  void ParseSectionHeaderTable(const QString& elfFile, QVector<SectionHeader>* table);
  void ParseSymbolTable(const QString& elfFile, QVector<Symbol>* table);
  void Disassemble(const QString& elfFile, std::uintptr_t address, std::uintptr_t size);

signals:
  void SignalParseSectionHeaderTableFinished(const QString& elfFile);
  void SignalParseSymbolTableFinished(const QString& elfFile);
  void SignalDisassemblyFinished(const QString dissAsm);

public:
  QString objdumpPath = "objdump";
  QString readelfPath = "readelf";
  QString nmPath      = "nm";

private:
  SectionHeaderTableParser* sectionParser_;
  SymbolTableParser*        symbolParser_;
};

} // namespace glove
