#include "Symbol.hxx"

namespace glove {

bool operator==(const Symbol& lhs, const Symbol& rhs) {
  return (lhs.name == rhs.name) && (lhs.section == rhs.section) &&
         (lhs.valueOrAddress == rhs.valueOrAddress) &&
         (lhs.sizeOrAlignment == rhs.sizeOrAlignment) && (lhs.flag0 == rhs.flag0) &&
         (lhs.flag1 == rhs.flag1) && (lhs.flag2 == rhs.flag2) && (lhs.flag3 == rhs.flag3) &&
         (lhs.flag4 == rhs.flag4) && (lhs.flag5 == rhs.flag5) && (lhs.flag6 == rhs.flag6);
}

} // namespace glove
