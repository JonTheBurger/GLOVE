#include "SectionHeader.hxx"

namespace glove {

bool operator==(const SectionHeader& lhs, const SectionHeader& rhs) {
  return (lhs.name == rhs.name) &&
         (lhs.type == rhs.type) &&
         (lhs.address == rhs.address) &&
         (lhs.offset == rhs.offset) &&
         (lhs.size == rhs.size) &&
         (lhs.entSize == rhs.entSize) &&
         (lhs.flags == rhs.flags) &&
         (lhs.link == rhs.link) &&
         (lhs.info == rhs.info) &&
         (lhs.alignment == rhs.alignment);
}

} // namespace glove
