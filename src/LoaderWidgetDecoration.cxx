#include "LoaderWidgetDecoration.hxx"

#include <QGraphicsOpacityEffect>
#include <QProgressBar>
#include <QResizeEvent>

LoaderWidgetDecoration::LoaderWidgetDecoration(QWidget* const parent)
    : QObject(parent)
    , parent_(parent)
    , bar_(new QProgressBar(parent))
    , opacity_(new QGraphicsOpacityEffect(parent)) {
  parent_->installEventFilter(this);

  opacity_->setOpacity(0.75);

  bar_->setMinimum(0);
  bar_->setMaximum(0);
  bar_->setGraphicsEffect(opacity_);
}

bool LoaderWidgetDecoration::eventFilter(QObject* const object, QEvent* const event) {
  if (object == parent_ && event->type() == QEvent::Resize) {
    FitSize();
    return true;
  }
  return false;
}

void LoaderWidgetDecoration::RunLoader(const bool isLoading) {
  FitSize();

  for (auto&& child : parent_->findChildren<QWidget*>()) {
    child->setEnabled(!isLoading);
  }
  bar_->setEnabled(isLoading);

  if (isLoading) {
    bar_->show();
  } else {
    bar_->hide();
  }
}

void LoaderWidgetDecoration::FitSize() {
  bar_->move(0, parent_->size().height() / 2 - bar_->size().height() / 2);
  bar_->resize(parent_->size().width(), bar_->size().height());
}
