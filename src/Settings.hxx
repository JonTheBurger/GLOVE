#pragma once

#include <QSettings>
#include <QString>
#include <QVector>
#include <QWidget>
#include <type_traits>

namespace glove {

class Settings {
public:
  Settings(QSettings* qsettings);
  ~Settings();
  void LoadWidgetSizePos(QWidget* widget) const;
  void SaveWindowSizePos(const QWidget& widget);

  template<typename T>
  void Save(const QString& key, const T& value) {
    qsettings_->setValue(key, value);
  }

  template<typename T>
  QVariant Load(const QString& key, const T defaultValue) {
    QVariant defaultVariant = defaultValue;
    return qsettings_->value(key, defaultVariant);
  }

  // Constrain type to any STL-like type Container<QString, ...>
  template<template<typename, typename...> class TContainer, typename T, typename... Args>
  typename std::enable_if<std::is_same<T, QString>::value, void>::type LoadStrings(
      const QString& key, TContainer<T, Args...>* paths) const {
    auto size = qsettings_->beginReadArray(key);
    for (decltype(size) i = 0; i < size; ++i) {
      qsettings_->setArrayIndex(i);
      paths->push_back(qsettings_->value("$").toString());
    }
    qsettings_->endArray();
  }

  // Constrain type to any STL-like type Container<QString, ...>
  template<template<typename, typename...> class TContainer, typename T, typename... Args>
  typename std::enable_if<std::is_same<T, QString>::value, void>::type SaveStrings(
      const QString& key, const TContainer<T, Args...>& paths) const {
    qsettings_->beginWriteArray(key);
    int i = 0;
    for (auto&& path : paths) {
      qsettings_->setArrayIndex(i);
      qsettings_->setValue("$", path);
      ++i;
    }
    qsettings_->endArray();
  }

private:
  QSettings* qsettings_;
};

} // namespace glove
