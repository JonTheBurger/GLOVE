#pragma once

#include <QString>
#include <QVector>

namespace glove {

enum class SectionHeaderFlags {
  None = 0,

  Unknown  = 1 << 0,
  Contents = 1 << 1,
  Alloc    = 1 << 2,
  Load     = 1 << 3,
  Readonly = 1 << 4,
  Code     = 1 << 5,
  Data     = 1 << 6,
};

inline SectionHeaderFlags operator|(SectionHeaderFlags lhs, SectionHeaderFlags rhs) {
  return static_cast<SectionHeaderFlags>(static_cast<int>(lhs) | static_cast<int>(rhs));
}

inline SectionHeaderFlags operator&(SectionHeaderFlags lhs, SectionHeaderFlags rhs) {
  return static_cast<SectionHeaderFlags>(static_cast<int>(lhs) & static_cast<int>(rhs));
}

inline SectionHeaderFlags& operator|=(SectionHeaderFlags& lhs, SectionHeaderFlags rhs) {
  return lhs = lhs | rhs;
}

inline SectionHeaderFlags& operator&=(SectionHeaderFlags& lhs, SectionHeaderFlags rhs) {
  return lhs = lhs & rhs;
}

struct SectionHeader {
  QString            name    = "";
  QString            type    = "";
  std::uintptr_t     address = 0;
  std::uintptr_t     offset  = 0;
  std::uintptr_t     size    = 0;
  std::uintptr_t     entSize = 0;
  SectionHeaderFlags flags;
  int                link      = 0;
  int                info      = 0;
  int                alignment = 0;
};

bool operator==(const SectionHeader& lhs, const SectionHeader& rhs);

} // namespace glove
