#pragma once

#include <QSqlQuery>
#include <QVector>

#include "SectionHeader.hxx"
#include "Symbol.hxx"

class QSqlDatabase;

namespace glove {

class DbAccess {
public:
  DbAccess(QSqlDatabase* db);
  ~DbAccess();
  bool Open(const QString& name = ":memory:");
  void Write(const QVector<SectionHeader>& headers);
  void Write(const QVector<Symbol>& symbolTable);
  QSqlQuery SectionHeaderTableQuery();
  QSqlQuery SymbolTableQuery();
  QSqlQuery AddressSizeQuery(const QString& functionName);

private:
  QSqlDatabase* db_;
};

} // namespace glove
