#pragma once

#include <QtGlobal>
#include <QString>
#include <QVector>

namespace glove {

struct Symbol {
  enum Flag0 : char {
    Neither = ' ',
    Local = 'l',
    Global = 'g',
    Both = '!',
    UniqueGlobal = 'u',
  };

  enum Flag1 : char {
    Strong = ' ',
    Weak = 'w',
  };

  enum Flag2 : char {
    NotConstructor = ' ',
    IsConstructor = 'C',
  };

  enum Flag3 : char {
    NotWarning = ' ',
    IsWarning = 'W',
  };

  enum Flag4 : char {
    Direct = ' ',
    Indirect = 'I',
    Relocated = 'i',
  };

  enum Flag5 : char {
    Static = ' ',
    Debug = 'd',
    Dynamic = 'D',
  };

  enum Flag6 : char {
    None = ' ',
    Function = 'F',
    File = 'f',
    Object = 'O',
  };

  QString name = "";
  QString section = "";
  quintptr valueOrAddress = 0;
  quintptr sizeOrAlignment = 0;
  Flag0 flag0 = Neither;
  Flag1 flag1 = Strong;
  Flag2 flag2 = NotConstructor;
  Flag3 flag3 = NotWarning;
  Flag4 flag4 = Direct;
  Flag5 flag5 = Static;
  Flag6 flag6 = None;
};

bool operator==(const Symbol& lhs, const Symbol& rhs);

} // namespace glove
