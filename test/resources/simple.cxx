#include <iostream>

using namespace std;

int myInt = 10;

void MyFunction() {
  cout << myInt << '\n';
}

int main() {
  MyFunction();
  return 0;
}
