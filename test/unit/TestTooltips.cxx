#include "catch.hpp"

#include <QString>
#include <map>

#include "SectionHeader.hxx"
#include "Symbol.hxx"
#include "Tooltips.hxx"

using namespace glove;

template<typename TContainer>
void AssertFlagsInDescriptions(const Tooltips tooltips, const TContainer& container) {
  for (auto&& flag : container) {
    const QString& Description = tooltips.FlagDescription(flag);
    const auto     Expected    = QString("(%1)").arg(static_cast<char>(flag));
    REQUIRE(Description.contains(Expected));
  }
}

SCENARIO("Tooltip description contains the flag they describe", "[tooltips]") {
  GIVEN("A tooltip manager") {
    const Tooltips tooltips;

    WHEN("A description is requested for a symbol flag in Flag0") {
      using SFlag      = Symbol::Flag0;
      const auto flags = {
        SFlag::Neither,
        SFlag::Local,
        SFlag::Global,
        SFlag::Both,
        SFlag::UniqueGlobal,
      };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a symbol flag in Flag1") {
      using SFlag      = Symbol::Flag1;
      const auto flags = { SFlag::Strong, SFlag::Weak };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a symbol flag in Flag2") {
      using SFlag      = Symbol::Flag2;
      const auto flags = { SFlag::NotConstructor, SFlag::IsConstructor };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a symbol flag in Flag3") {
      using SFlag      = Symbol::Flag3;
      const auto flags = { SFlag::NotWarning, SFlag::IsWarning };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a symbol flag in Flag4") {
      using SFlag      = Symbol::Flag4;
      const auto flags = { SFlag::Direct, SFlag::Indirect, SFlag::Relocated };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a symbol flag in Flag5") {
      using SFlag      = Symbol::Flag5;
      const auto flags = { SFlag::Static, SFlag::Debug, SFlag::Dynamic };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a symbol flag in Flag6") {
      using SFlag      = Symbol::Flag6;
      const auto flags = { SFlag::None, SFlag::Function, SFlag::File, SFlag::Object };

      THEN("A description containing said flag is returned") {
        AssertFlagsInDescriptions(tooltips, flags);
      }
    }

    WHEN("A description is requested for a section header flag") {
      using SFlag                                         = SectionHeaderFlags;
      const std::map<SFlag, const QString> flagsToKeyword = {
        { SFlag::Contents, "CONTENTS" },
        { SFlag::Alloc, "ALLOC" },
        { SFlag::Load, "LOAD" },
        { SFlag::Readonly, "READONLY" },
        { SFlag::Data, "DATA" },
      };

      THEN("A description containing said flag is returned") {
        for (auto&& kvp : flagsToKeyword) {
          auto&&         flag        = kvp.first;
          auto&&         keyword     = kvp.second;
          const QString& Description = tooltips.FlagDescription(flag);
          REQUIRE(Description.contains(keyword));
        }
      }
    }

    WHEN("A flag represented by <space> is not an ordinary symbol") {
      auto&& ordinaryDescription = tooltips.FlagDescription(' ');

      AND_WHEN("The flag specifically describes a symbol that is neither a local nor global") {
        auto&& neitherDescription = tooltips.FlagDescription(Symbol::Flag0::Neither);

        THEN("The description returned does not describe an \"ordinary flag\"") {
          REQUIRE(ordinaryDescription != neitherDescription);
        }
      }

      AND_WHEN("The flag describes a strong symbol") {
        auto&& strongDescription = tooltips.FlagDescription(Symbol::Flag1::Strong);

        THEN("The description returned does not describe an \"ordinary flag\"")
        REQUIRE(ordinaryDescription != strongDescription);
      }
    }
  }
}
