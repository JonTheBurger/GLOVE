#include <iostream>
#include "catch.hpp"

#include <QString>
#include <QStringRef>
#include <QVector>

#include "Symbol.hxx"
#include "SymbolTableParser.hxx"

TEST_CASE() {
  using namespace glove;
  //  QString line = "__bss_start         |0000000000601064|   B  |            NOTYPE|                |     |.bss";
  QString line = " |  |   |     |";

  int x = line.lastIndexOf('|');
  std::cout << x << '\n';

  int y = line.lastIndexOf('|', x - 1);
  std::cout << y << '\n';

  int z = line.lastIndexOf('|', y - 1);
  std::cout << z << '\n';

  int a = line.lastIndexOf('|', z - 1);
  std::cout << a << '\n';
}
