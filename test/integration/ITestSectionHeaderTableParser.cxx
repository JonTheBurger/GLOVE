#include "catch.hpp"

#include <QProcess>
#include <QString>
#include <QVector>

#include "SectionHeader.hxx"
#include "SectionHeaderTableParser.hxx"

const QString ProjectDir        = PROJECT_DIR;
const QString SimpleElfFilePath = ProjectDir + "/test/resources/simple.elf";

TEST_CASE("Parsing simple.elf returns the expected section headers",
          "[section header table parser][parser][section][integration]") {
  using namespace glove;

  QProcess      objdump;
  const QString Name = "objdump";
  QStringList   args;
  args << "-Ch" << SimpleElfFilePath;
  objdump.start(Name, args);
  objdump.waitForFinished();
  auto&& output = QString(objdump.readAllStandardOutput());

  // 2 .note.gnu.build-id 00000024  0000000000400274  0000000000400274  00000274  2**2
  // CONTENTS, ALLOC, LOAD, READONLY, DATA
  SectionHeader header2;
  header2.name = ".note.gnu.build-id";
  header2.size = 0x24;
  //  header2.vma       = 0x400274;
  //  header2.lma       = 0x400274;
  //  header2.fileOff   = 0x274;
  header2.alignment = 4;
  header2.flags     = SectionHeaderFlags::Contents | SectionHeaderFlags::Alloc |
                  SectionHeaderFlags::Load | SectionHeaderFlags::Readonly |
                  SectionHeaderFlags::Data;

  // 7 .gnu.version_r 00000040  00000000004004a0  00000000004004a0  000004a0  2**3
  // CONTENTS, ALLOC, LOAD, READONLY, DATA
  SectionHeader header7;
  header7.name = ".gnu.version_r";
  header7.size = 0x40;
  //  header7.vma       = 0x400520;
  //  header7.lma       = 0x400520;
  //  header7.fileOff   = 0x520;
  header7.alignment = 8;
  header7.flags     = SectionHeaderFlags::Contents | SectionHeaderFlags::Alloc |
                  SectionHeaderFlags::Load | SectionHeaderFlags::Readonly |
                  SectionHeaderFlags::Data;

  SectionHeaderTableParser parser;
  //  QVector<SectionHeader> sectionHeaderTable = parser.FromObjdumpOutput(output);

  //  REQUIRE(header2 == sectionHeaderTable[2]);
  //  REQUIRE(header7 == sectionHeaderTable[7]);
}
