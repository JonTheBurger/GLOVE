#include <QFile>
#include <QProcess>
#include <QRegularExpression>
#include <QString>
#include <QStringRef>
#include <QVector>
#include <iostream>
#include "catch.hpp"

#include "Symbol.hxx"
#include "SymbolTableParser.hxx"

using namespace glove;

const QString ProjectDir        = PROJECT_DIR;
const QString SimpleElfFilePath = ProjectDir + "/test/resources/simple.elf";
