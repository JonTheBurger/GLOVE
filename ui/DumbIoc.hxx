#include <type_traits>
#include <unordered_map>

class DumbIoc {
public:
  template<typename T>
  static void Register(T instance) {
    Instance<T>() = instance;
  }

  template<typename T>
  static T Get() {
    return Instance<T>();
  }

  template<typename T,
           typename std::enable_if_t<std::is_default_constructible<T>::value &&
                                     std::is_move_assignable<T>::value>>
  static void Unregister() {
    T t;
    Instance<T>() = std::move(t);
  }

  template<typename T>
  static void RegisterById(T instance, int id) {
    InstanceById<T>().insert({ id, instance });
  }

  template<typename T, int id>
  static T Get() {
    return InstanceById<T>().at(id);
  }

  template<typename T>
  static void Unregister(int id) {
    InstanceById<T>().erase(InstanceById<T>().find(id));
  }

private:
  template<typename T>
  static T& Instance() {
    static T instance;
    return instance;
  }

  template<typename T>
  static std::unordered_map<int, T>& InstanceById() {
    static std::unordered_map<int, T> instanceById{};
    return instanceById;
  }
};
