#pragma once

#include "ui_TestForm.h"

namespace glove {

class TestForm : public QWidget {
  Q_OBJECT

public:
  explicit TestForm(QWidget* parent = nullptr);
  void AppendText(const QString& string);

private:
  Ui::TestForm ui_;
};

} // namespace glove
