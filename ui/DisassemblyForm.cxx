#include <QDebug>
#include <QSortFilterProxyModel>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QSqlRecord>
#include <cstdint>

#include "BinUtils.hxx"
#include "DbAccess.hxx"
#include "DisassemblyForm.hxx"
#include "DumbIoc.hxx"

namespace glove {

DisassemblyForm::DisassemblyForm(QWidget* parent)
    : QWidget(parent)
    , symbolTable_(DumbIoc::Get<decltype(symbolTable_)>())
    , binUtils_(DumbIoc::Get<decltype(binUtils_)>())
    , db_(DumbIoc::Get<decltype(db_)>())
    , elfFile_(DumbIoc::Get<decltype(elfFile_), 'e'>()) {
  ui_.setupUi(this);
  ui_.splitter->setStretchFactor(0, 25);
  ui_.splitter->setStretchFactor(1, 75);
  connect(binUtils_, &BinUtils::SignalDisassemblyFinished, this, [this](const QString dissAsm) {
    ui_.asmTextEdit->setText(dissAsm);
  });
}

void DisassemblyForm::LoadFunctions() {
  QSqlQuery functionQuery;
  functionQuery.prepare("SELECT * FROM Symbols WHERE flag6=? ORDER BY name");
  functionQuery.addBindValue(static_cast<int>(Symbol::Flag6::Function));
  if (!functionQuery.exec()) { qDebug() << "WARN:" << functionQuery.lastError().text() << '\n'; }

  model_.setQuery(functionQuery);
  ui_.functionListView->setModel(&model_);

  connect(ui_.functionListView->selectionModel(),
          &QItemSelectionModel::currentRowChanged,
          this,
          [this](QModelIndex idx) {
            QString functionName = model_.data(idx).toString();
            QSqlQuery addressQuery = db_->AddressSizeQuery(functionName);
            addressQuery.exec();
            addressQuery.next();
            QSqlRecord record = addressQuery.record();
            const std::uintptr_t address = record.value(0).toULongLong();
            const std::uintptr_t size = record.value(1).toULongLong();

            binUtils_->Disassemble(*elfFile_, address, size);
          });
}

} // namespace glove
