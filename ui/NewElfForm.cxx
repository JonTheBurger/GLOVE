#include <QFileDialog>

#include "BinUtils.hxx"
#include "DumbIoc.hxx"
#include "NewElfForm.hxx"
#include "Settings.hxx"

namespace glove {

NewElfForm::NewElfForm(QWidget* parent)
    : QWidget(parent)
    , binutils_(DumbIoc::Get<decltype(binutils_)>())
    , settings_(DumbIoc::Get<decltype(settings_)>())
    , elfFile_(DumbIoc::Get<decltype(elfFile_), 'e'>())
    , mapFile_(DumbIoc::Get<decltype(mapFile_), 'm'>()) {
  ui_.setupUi(this);

  connect(ui_.objdumpLineEdit, &QLineEdit::textChanged, this, &NewElfForm::UpdateNextButtonEnabled);
  connect(ui_.readelfLineEdit, &QLineEdit::textChanged, this, &NewElfForm::UpdateNextButtonEnabled);
  connect(ui_.elfFileLineEdit, &QLineEdit::textChanged, this, &NewElfForm::UpdateNextButtonEnabled);
  connect(ui_.mapFileLineEdit, &QLineEdit::textChanged, this, &NewElfForm::UpdateNextButtonEnabled);

  connect(ui_.objdumpBrowseButton, &QPushButton::clicked, this, [this] {
    QString objdumpPath = QFileDialog::getOpenFileName(
        this, tr("Path to objdump executable"), "", tr("objdump (*objdump*);;All Files (*.*)"));
    if (objdumpPath != "") { ui_.objdumpLineEdit->setText(objdumpPath); }
  });
  connect(
      ui_.objdumpBrowseButton, &QPushButton::clicked, this, &NewElfForm::UpdateNextButtonEnabled);

  connect(ui_.readelfBrowseButton, &QPushButton::clicked, this, [this] {
    QString readelfPath = QFileDialog::getOpenFileName(
        this, tr("Path to readelf executable"), "", tr("readelf (*readelf*);;All Files (*.*)"));
    if (readelfPath != "") { ui_.readelfLineEdit->setText(readelfPath); }
  });
  connect(
      ui_.readelfBrowseButton, &QPushButton::clicked, this, &NewElfForm::UpdateNextButtonEnabled);

  connect(ui_.elfFileBrowseButton, &QPushButton::clicked, this, [this] {
    QString elfFile = QFileDialog::getOpenFileName(
        this, tr("Path to elf file"), "", tr("Elf File (*.elf *.axf *.out);;All Files (*.*)"));
    if (elfFile != "") { ui_.elfFileLineEdit->setText(elfFile); }
  });
  connect(
      ui_.elfFileBrowseButton, &QPushButton::clicked, this, &NewElfForm::UpdateNextButtonEnabled);

  connect(ui_.mapFileBrowseButton, &QPushButton::clicked, this, [this] {
    QString mapFile = QFileDialog::getOpenFileName(
        this, tr("Path to map file"), "", tr("Map File (*.map);;All Files (*.*)"));
    if (mapFile != "") { ui_.mapFileLineEdit->setText(mapFile); }
  });
  connect(
      ui_.mapFileBrowseButton, &QPushButton::clicked, this, &NewElfForm::UpdateNextButtonEnabled);

  connect(ui_.nextButton, &QPushButton::clicked, this, [this] {
    *elfFile_ = ui_.elfFileLineEdit->text();
    *mapFile_ = ui_.mapFileLineEdit->text();
    binutils_->objdumpPath = ui_.objdumpLineEdit->text();
    binutils_->readelfPath = ui_.readelfLineEdit->text();
    emit SignalNextForm();
  });

  ui_.elfFileLineEdit->setText(settings_->Load<QString>("Elffile", "").toString());
  ui_.mapFileLineEdit->setText(settings_->Load<QString>("Mapfile", "").toString());
} // namespace glove

NewElfForm::~NewElfForm() {
  settings_->Save("Elffile", ui_.elfFileLineEdit->text());
  settings_->Save("Mapfile", ui_.mapFileLineEdit->text());
}

void NewElfForm::UpdateNextButtonEnabled() {
  bool isConfigured = (ui_.mapFileLineEdit->text() != "") ||
                      (ui_.elfFileLineEdit->text() != "" && ui_.objdumpLineEdit->text() != "");
  ui_.nextButton->setEnabled(isConfigured);
}

} // namespace glove
