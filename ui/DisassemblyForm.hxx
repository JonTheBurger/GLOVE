#pragma once

#include <QSqlQueryModel>
#include <QVector>

#include "Symbol.hxx"
#include "ui_DisassemblyForm.h"

namespace glove {

class BinUtils;
class DbAccess;
class DisassemblyForm : public QWidget {
  Q_OBJECT

public:
  explicit DisassemblyForm(QWidget* parent = nullptr);
  void LoadFunctions();

private:
  Ui::DisassemblyForm ui_;
  QVector<Symbol>* symbolTable_;
  BinUtils* binUtils_;
  DbAccess* db_;
  QString* elfFile_;
  QSqlQueryModel model_;
};

} // namespace glove
