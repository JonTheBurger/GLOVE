#include <QBarCategoryAxis>
#include <QBarSet>
#include <QDebug>
#include <QPercentBarSeries>
#include <QProcess>
#include <QSortFilterProxyModel>
#include <QSqlError>
#include <QSqlQueryModel>
#include <QString>

#include "DumbIoc.hxx"
#include "SectionsForm.hxx"

namespace glove {

SectionsForm::SectionsForm(QWidget* parent)
    : QWidget(parent)
    , sectionHeaderTable_(DumbIoc::Get<decltype(sectionHeaderTable_)>()) {
  ui_.setupUi(this);
}

void SectionsForm::LoadSectionHeaderTable() {
  LoadChart(tr("Section Headers"));
}

void SectionsForm::LoadChart(const QString& title) {
  QT_CHARTS_USE_NAMESPACE;
  auto* chart = new QChart();
  chart->setTheme(QChart::ChartThemeDark);
  ui_.chartView->setChart(chart);

  auto* series = new QPercentBarSeries(chart);
  auto* axis = new QBarCategoryAxis(chart);
  axis->append("");

  for (auto&& header : *sectionHeaderTable_) {
    auto* barSet = new QBarSet(header.name, series);
    *barSet << header.size;
    series->append(barSet);
  }

  chart->addSeries(series);
  chart->setTitle(title);
  chart->setAxisX(axis, series);
  chart->setAnimationOptions(QChart::SeriesAnimations);
  chart->legend()->setVisible(true);
  chart->legend()->setShowToolTips(true);
  chart->legend()->setAlignment(Qt::AlignLeft);

  ui_.chartView->setRenderHint(QPainter::Antialiasing);
}

} // namespace glove
