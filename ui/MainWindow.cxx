#include <QDebug>
#include <QProcess>
#include <QString>

#include "BinUtils.hxx"
#include "DbAccess.hxx"
#include "MainWindow.hxx"
#include "SectionHeaderTableParser.hxx"
#include "SymbolTableParser.hxx"

namespace glove {

MainWindow::MainWindow(BinUtils* binutils,
                       DbAccess* db,
                       Settings* settings,
                       Tooltips* tooltips,
                       QVector<Symbol>* symbolTable,
                       QVector<SectionHeader>* sectionHeaderTable,
                       QString* elfFile,
                       QString* mapFile,
                       QWidget* parent)
    : QMainWindow(parent)
    , binutils_(binutils)
    , db_(db)
    , settings_(settings)
    , tooltips_(tooltips)
    , symbolTable_(symbolTable)
    , sectionHeaderTable_(sectionHeaderTable)
    , elfFile_(elfFile)
    , mapFile_(mapFile) {
  ui_.setupUi(this);

  connect(ui_.newElfForm, &NewElfForm::SignalNextForm, this, [this] {
    ui_.stackedWidget->setCurrentWidget(ui_.tabWidget);
    binutils_->ParseSectionHeaderTable(*this->elfFile_, sectionHeaderTable_);
    binutils_->ParseSymbolTable(*this->elfFile_, symbolTable_);
  });

  connect(binutils_,
          &BinUtils::SignalParseSectionHeaderTableFinished,
          this,
          [this](const QString& parsedElfFile) {
            (void)parsedElfFile;
            db_->Open(parsedElfFile + ".db");
            db_->Write(*this->sectionHeaderTable_);
            ui_.sectionsTab->LoadSectionHeaderTable();
          });

  connect(binutils_,
          &BinUtils::SignalParseSymbolTableFinished,
          this,
          [this](const QString& parsedElfFile) {
            (void)parsedElfFile;
            db_->Open(parsedElfFile + ".db");
            db_->Write(*this->symbolTable_);
            ui_.symbolTableTab->LoadSymbolTable();
            ui_.disassemblyTab->LoadFunctions();
          });
}

} // namespace glove
