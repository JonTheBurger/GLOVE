#pragma once

#include "ui_DebugForm.h"

namespace glove {

class DebugForm : public QWidget {
  Q_OBJECT

public:
  explicit DebugForm(QWidget* parent = nullptr);

private:
  Ui::DebugForm ui_;
};

} // namespace glove
