#pragma once

#include <QtCharts/QChartView>

#include "SectionHeader.hxx"
#include "Symbol.hxx"
#include "ui_MainWindow.h"

namespace glove {

class BinUtils;
class DbAccess;
class Settings;
class Tooltips;

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(BinUtils* binutils,
                      DbAccess* db,
                      Settings* settings,
                      Tooltips* tooltips,
                      QVector<Symbol>* symbolTable,
                      QVector<SectionHeader>* sectionHeaderTable,
                      QString* elfFile_,
                      QString* mapFile_,
                      QWidget* parent = nullptr);

private:
  Ui::MainWindow ui_;
  BinUtils* binutils_;
  DbAccess* db_;
  Settings* settings_;
  Tooltips* tooltips_;
  QVector<Symbol>* symbolTable_;
  QVector<SectionHeader>* sectionHeaderTable_;
  QString* elfFile_;
  QString* mapFile_;
};

} // namespace glove
