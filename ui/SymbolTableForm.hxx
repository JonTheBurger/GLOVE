#pragma once

#include <QVector>

#include "Symbol.hxx"
#include "ui_SymbolTableForm.h"

namespace glove {

class DbAccess;
class SymbolTableForm : public QWidget {
  Q_OBJECT

public:
  explicit SymbolTableForm(QWidget* parent = nullptr);
  void LoadSymbolTable();

protected:
  void resizeEvent(QResizeEvent* event) override;

private:
  Ui::SymbolTableForm ui_;
  QVector<glove::Symbol>* symbolTable_;
  glove::DbAccess* db_;
};

} // namespace glove
