#include "TestForm.hxx"

namespace glove {

TestForm::TestForm(QWidget* parent)
    : QWidget(parent) {
  ui_.setupUi(this);
}

void TestForm::AppendText(const QString& string) {
  ui_.testTextEdit->append(string);
}

} // namespace glove
