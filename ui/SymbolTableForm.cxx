#include <QDebug>
#include <QSortFilterProxyModel>
#include <QSqlError>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QString>

#include "DbAccess.hxx"
#include "DumbIoc.hxx"
#include "SymbolTableForm.hxx"

namespace glove {

SymbolTableForm::SymbolTableForm(QWidget* parent)
    : QWidget(parent)
    , symbolTable_(DumbIoc::Get<decltype(symbolTable_)>())
    , db_(DumbIoc::Get<decltype(db_)>()) {
  ui_.setupUi(this);
}

void SymbolTableForm::resizeEvent(QResizeEvent* event) {
  // Column type weights in fractions
  constexpr int StringColumnRatio = 5;
  constexpr int NumberColumnRatio = 12;
  constexpr int FlagColumnRatio = 17;

  const int Width = ui_.symbolTableView->width();

  ui_.symbolTableView->setColumnWidth(0, Width / StringColumnRatio);
  ui_.symbolTableView->setColumnWidth(1, Width / StringColumnRatio);
  ui_.symbolTableView->setColumnWidth(2, Width / NumberColumnRatio);
  ui_.symbolTableView->setColumnWidth(3, Width / NumberColumnRatio);
  ui_.symbolTableView->setColumnWidth(4, Width / FlagColumnRatio);
  ui_.symbolTableView->setColumnWidth(5, Width / FlagColumnRatio);
  ui_.symbolTableView->setColumnWidth(6, Width / FlagColumnRatio);
  ui_.symbolTableView->setColumnWidth(7, Width / FlagColumnRatio);
  ui_.symbolTableView->setColumnWidth(8, Width / FlagColumnRatio);
  ui_.symbolTableView->setColumnWidth(9, Width / FlagColumnRatio);
  ui_.symbolTableView->setColumnWidth(10, Width / FlagColumnRatio);

  QWidget::resizeEvent(event);
}

void SymbolTableForm::LoadSymbolTable() {
  db_->Write(*symbolTable_);

  QSqlQuery query;
  query.prepare("SELECT * from Symbols");
  if (!query.exec()) { qDebug() << "WARN:" << query.lastError().text() << '\n'; }

  auto model = new QSqlQueryModel(this);
  model->setQuery(query);
  model->setHeaderData(0, Qt::Orientation::Horizontal, tr("Name"));
  model->setHeaderData(1, Qt::Orientation::Horizontal, tr("Section"));
  model->setHeaderData(2, Qt::Orientation::Horizontal, tr("Address/Value"));
  model->setHeaderData(3, Qt::Orientation::Horizontal, tr("Size/Alignment"));
  model->setHeaderData(4, Qt::Orientation::Horizontal, tr("{lg!u}"));
  model->setHeaderData(5, Qt::Orientation::Horizontal, tr("{w}"));
  model->setHeaderData(6, Qt::Orientation::Horizontal, tr("{C}"));
  model->setHeaderData(7, Qt::Orientation::Horizontal, tr("{W}"));
  model->setHeaderData(8, Qt::Orientation::Horizontal, tr("{Ii}"));
  model->setHeaderData(9, Qt::Orientation::Horizontal, tr("{dD}"));
  model->setHeaderData(10, Qt::Orientation::Horizontal, tr("{FfO}"));

  auto proxyModel = new QSortFilterProxyModel(this);
  proxyModel->setSourceModel(model);

  ui_.symbolTableView->setModel(proxyModel);
}

} // namespace glove
