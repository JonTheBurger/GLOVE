#pragma once

#include <QVector>

#include "SectionHeader.hxx"
#include "ui_SectionsForm.h"

namespace glove {

class SectionsForm : public QWidget {
  Q_OBJECT

public:
  explicit SectionsForm(QWidget* parent = nullptr);

  void LoadSectionHeaderTable();
  void LoadChart(const QString& title);

private:
  Ui::SectionsForm ui_;
  QVector<glove::SectionHeader>* sectionHeaderTable_;
};

} // namespace glove
