#pragma once

#include "ui_CallTreeForm.h"

namespace glove {

class CallTreeForm : public QWidget {
  Q_OBJECT

public:
  explicit CallTreeForm(QWidget* parent = nullptr);

private:
  Ui::CallTreeForm ui_;
};

} // namespace glove
