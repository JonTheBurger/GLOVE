#pragma once

#include "ui_NewElfForm.h"

namespace glove {

class BinUtils;
class Settings;

class NewElfForm : public QWidget {
  Q_OBJECT

public:
  explicit NewElfForm(QWidget* parent = nullptr);
  ~NewElfForm();

signals:
  void SignalNextForm();

private:
  Ui::NewElfForm ui_;
  BinUtils* binutils_;
  Settings* settings_;
  QString* elfFile_;
  QString* mapFile_;

  void UpdateNextButtonEnabled();
  void LoadSettings();
  void SaveSettings();
};

} // namespace glove
