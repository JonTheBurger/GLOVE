add_executable(Glove main.cxx)
target_link_libraries(Glove
    PRIVATE
    GloveUi
    GloveResources
)
