#include <QApplication>
#include <QDebug>
#include <QFile>
#include <QSettings>
#include <QSqlDatabase>
#include <QTranslator>

#include "BinUtils.hxx"
#include "DbAccess.hxx"
#include "DumbIoc.hxx"
#include "MainWindow.hxx"
#include "SectionHeaderTableParser.hxx"
#include "Settings.hxx"
#include "SymbolTableParser.hxx"
#include "Tooltips.hxx"

int main(int argc, char* argv[]) {
  using namespace glove;
  QApplication app(argc, argv);

  // Setup Settings
  QApplication::setOrganizationName("Glove Project");
  QApplication::setOrganizationDomain("https://github.com/JonTheBurger/GLOVE");
  QApplication::setApplicationName("Glove");
  QSettings qsettings;
  Settings settings(&qsettings);

  // Setup Style
  Q_INIT_RESOURCE(resources);
  QFile styleFile(":/style/QDarkStyleSheet/style.qss");
  if (!styleFile.open(QFile::ReadOnly | QFile::Text)) {
    qDebug() << "ERROR: Style open failure";
  }
  app.setStyleSheet(styleFile.readAll());

  // Setup Translations
  QTranslator translator;
  // translator.load(QLocale(), "filename", "prefix", "directory", "suffix");
  app.installTranslator(&translator);

  // Setup Database
  QSqlDatabase db = QSqlDatabase::addDatabase("QSQLITE");
  DbAccess dba(&db);

  // Setup GLOVE data
  SectionHeaderTableParser sectionParser;
  SymbolTableParser symbolParser;
  BinUtils binutils(&sectionParser, &symbolParser);
  Tooltips tooltips;
  QVector<Symbol> symbolTable;
  QVector<SectionHeader> sectionHeaderTable;
  QString elfFile;
  QString mapFile;

  // Setup DumbIoc
  DumbIoc::Register(&settings);
  DumbIoc::Register(&translator);
  DumbIoc::Register(&dba);
  DumbIoc::Register(&sectionParser);
  DumbIoc::Register(&symbolParser);
  DumbIoc::Register(&binutils);
  DumbIoc::Register(&tooltips);
  DumbIoc::Register(&symbolTable);
  DumbIoc::Register(&sectionHeaderTable);
  DumbIoc::RegisterById(&elfFile, 'e');
  DumbIoc::RegisterById(&mapFile, 'm');

  MainWindow window(&binutils,
                    &dba,
                    &settings,
                    &tooltips,
                    &symbolTable,
                    &sectionHeaderTable,
                    &elfFile,
                    &mapFile);
  window.show();
  return app.exec();
}
